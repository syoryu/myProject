var count = 0;
var cells;
// ブロックのパターン
var blocks = {
  i: {
    class: "i",
    pattern: [
      [1, 1, 1, 1]
    ]
  },
  o: {
    class: "o",
    pattern: [
      [1, 1],
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0],
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1],
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0],
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0],
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  }
};

row_size = 8//20
col_size = 10//10

loadTable();
setInterval(function () {
  count++;
  //document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
  //alert(hasFallingBlock())
  // ブロックが積み上がり切っていないかのチェック
  //for (var row = 0; row < 1; row++) {
  for (var col = 0; col < 10; col++) {
    if (cells[0][col].className !== "" && cells[1][col].className !== "" && cells[2][col].className !== "") {
      alert("game over");
    }
  }
  //}
  if (hasFallingBlock()) { // 落下中のブロックがあるか確認する
    fallBlocks();// あればブロックを落とす
  } else { // なければ
    deleteRow();// そろっている行を消す
    generateBlock();// ランダムにブロックを作成する
  }
}, 1000);

// キーボードイベントを監視する
document.addEventListener("keydown", onKeyDown);
// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
  if (event.keyCode === 37) {
    moveLeft();
  } else if (event.keyCode === 39) {
    moveRight();
  }
}
/* ------ ここから下は関数の宣言部分 ------ */
function loadTable() {
  cells = [];
  var td_array = document.getElementsByTagName("td");
  var index = 0;
  for (var row = 0; row < row_size; row++) {
    cells[row] = [];
    for (var col = 0; col < col_size; col++) {
      cells[row][col] = td_array[index];
      index++;
    }
  }
}

function fallBlocks() {
  // 1. 底についていないか？
  for (var col = 0; col < col_size; col++) {
    if (cells[row_size-1][col].blockNum === fallingBlockNum) {
      //alert("底についていないか？")
      isFalling = false;
      return; // 一番下の行にブロックがいるので落とさない
    }
  }
  // 2. 1マス下に別のブロックがないか？
  for (var row = row_size-2; row >= 0; row--) {
    for (var col = 0; col < col_size; col++) {
      if (cells[row][col].blockNum === fallingBlockNum && cells[row][col].blockNum !== cells[row+1][col].blockNum) {
        if (cells[row + 1][col].className !== ""){
          //alert(cells[row][col].blockNum+" "+fallingBlockNum+" "+row+" "+col+" 1マス下に別のブロックがないか？")
          isFalling = false;
          return; // 一つ下のマスにブロックがいるので落とさない
        }
      }
    }
  }
  // 下から二番目の行から繰り返しクラスを下げていく
  for (var row = row_size-2; row >= 0; row--) {
    for (var col = 0; col < col_size; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}


var isFalling = 0;
function hasFallingBlock() {
  // 落下中のブロックがあるか確認する
  return isFalling;
}

function deleteRow() {
  // そろっている行を消す
  for (var row = row_size-1; row >= 0; row--) {
    var canDelete = true;
    for (var col = 0; col < col_size; col++) {
      if (cells[row][col].className === "") {
        canDelete = false;
      }
    }
    if (canDelete) {
      // 1行消す
      for (var col = 0; col < col_size; col++) {
        cells[row][col].className = "";
      }
      // 上の行のブロックをすべて1マス落とす
      for (var downRow = row - 1; downRow >= 0; downRow--) {
        alert(downRow)
        for (var col = 0; col < col_size; col++) {
          cells[downRow + 1][col].className = cells[downRow][col].className;
          cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
          cells[downRow][col].className = "";
          cells[downRow][col].blockNum = null;
        }
      }
    }
  }
}

var fallingBlockNum = 0;
function generateBlock() {
  // ランダムにブロックを生成する
  // 1. ブロックパターンからランダムに一つパターンを選ぶ
  var keys = Object.keys(blocks);
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;
  // 2. 選んだパターンをもとにブロックを配置する
  var pattern = nextBlock.pattern;
  for (var row = 0; row < pattern.length; row++) {
    for (var col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }
  // 3. 落下中のブロックがあるとする
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}

function moveRight() {
  // ブロックを右に移動させる
  for (var row = 0; row < row_size; row++) {
    if (row < row_size-1 && (cells[row][9].className !== "" || cells[row+1][9].className !== "")){
      return
    }
    for (var col = col_size-2; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

function moveLeft() {
  // ブロックを左に移動させる
  for (var row = 0; row < row_size; row++) {
    if (row < row_size-1 && (cells[row][0].className !== "" || cells[row+1][0].className !== "")){
      return
    }
    for (var col = 1; col < col_size; col++) {
      if (cells[row][col].blockNum === fallingBlockNum ) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
